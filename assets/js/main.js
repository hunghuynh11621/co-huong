$(document).ready(function() {
  $("#image-uploader").fileinput({
    maxFileCount: 1,
    allowedFileTypes: ["image"],
    showUpload: true,
    showPreview: true,
  });
});